from urllib.request import urlopen
import json



def get_boredapi_activities():
    url = 'https://raw.githubusercontent.com/drewthoennes/Bored-API/master/activities.json'
    activities = urlopen(url)
    activity_list = []
    for activity in activities:
        activity = json.loads(activity.decode('utf8').rstrip())
        activity_list.append(activity)

    activities.close()
    return activity_list
