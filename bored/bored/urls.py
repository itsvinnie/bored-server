from django.contrib import admin
from django.urls import path, re_path

from activities.views import activity_single, activity_page


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', activity_single),
    re_path(r'^(?P<pk>\d+)$', activity_page)
]
