from django.db import models

class Activity(models.Model):
    activity = models.CharField(max_length=100)
    accessibility = models.FloatField(default=0.0)
    type = models.CharField(max_length=20, choices=(
            ("education", "education"),
            ("recreational", "recreational"),
            ("social", "social"),
            ("diy", "diy"),
            ("charity", "charity"),
            ("cooking", "cooking"),
            ("relaxation", "relaxation"),
            ("music", "music"),
            ("busywork", "busywork"),
        )
    )
    participants = models.PositiveSmallIntegerField(default=1)
    price = models.FloatField(default=0)
    link = models.CharField(max_length=200, null=True, blank=True)
    key = models.PositiveIntegerField(null=True, blank=True)
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.activity} - {self.type}"

