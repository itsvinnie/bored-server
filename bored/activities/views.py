from django.shortcuts import render

from django.http import HttpResponse
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from activities.models import Activity
from activities.serializers import ActivitySerializer

import random
 
 
@csrf_exempt
def activity_single(request):
    if request.method == 'GET':
        parameters = {field_name: value for field_name, value in request.GET.items()
                    if value and field_name in ('accessibility', 'type', 'participants', 'price')}
        
        parameters['enabled'] = True

        activity_list = Activity.objects.filter(**parameters).values_list('id', flat=True)

        if not activity_list:
            return JsonResponse({}, status=status.HTTP_404_NOT_FOUND)

        random_activity = random.choice(activity_list)
        
        activity = Activity.objects.get(pk=random_activity)

        activity_serializer = ActivitySerializer(activity)
        return JsonResponse(activity_serializer.data)
 
    elif request.method == 'POST':
        activity_data = JSONParser().parse(request)
        activity_serializer = ActivitySerializer(data=activity_data)
        if activity_serializer.is_valid():
            activity_serializer.save()
            return JsonResponse(activity_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(activity_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
def activity_page(request, pk):
    try:
        activity = Activity.objects.get(pk=pk)
    except Activity.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    
    if request.method == 'GET':
        if not activity.enabled:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        activity_serializer = ActivitySerializer(activity)
        return JsonResponse(activity_serializer.data)

