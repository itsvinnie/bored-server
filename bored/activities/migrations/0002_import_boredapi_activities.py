from django.db import migrations, models

from urllib.request import urlopen
import json


def get_boredapi_activities():
    url = 'https://raw.githubusercontent.com/drewthoennes/Bored-API/master/activities.json'
    activities = urlopen(url)
    activity_list = []
    for activity in activities:
        activity = json.loads(activity.decode('utf8').rstrip())
        activity_list.append(activity)

    activities.close()
    return activity_list


def import_boredapi_activities(apps, schema_editor):
    activities = get_boredapi_activities()

    Activity = apps.get_model("activities", "Activity")
    Activity.objects.bulk_create([
        Activity(**activity_fields) for activity_fields in activities
    ])


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0001_initial'),
    ]

    operations = operations = [
        migrations.RunPython(import_boredapi_activities),
    ]
