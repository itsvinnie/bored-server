# Bored webapp

## Description
This web app returns a random activity in json format.

It is based on [boredapi](https://github.com/drewthoennes/Bored-API). The backend uses Django and the database is PostgreSQL. The database is populated using activities taken from https://github.com/drewthoennes/Bored-API/blob/master/activities.json

## Database

ENGINE: django.db.backends.postgresql,
NAME: bored,
USER: panda,
PASSWORD: may11,
HOST: 127.0.0.1,
PORT: 5432,

Install PostgreSQL
```
brew update
brew install postgresql
```

Start the database server
```
pg_ctl -D /usr/local/var/postgres start
```

Create the database
```
createdb bored
```

Create the database user
```
createuser panda
```
```
psql> GRANT ALL PRIVILEGES ON DATABASE bored TO panda;
psql> ALTER ROLE panda SET client_encoding TO 'utf8';
```

## Django server
Create a virtual environment, for example, with virtualenvwrapper:
```
mkvirtualenv -p python3 bored
```

Install the packages needed
```
python -m pip install -r requirements.txt
```

Run the migrations to create the database tables (0001) and import boredapi's activities (0002)
```
python manage.py migrate
```

Run the development server (make sure port 8000 on localhost is available)
```
python manage.py runserver
```

## Usage

### with the bored-client (recommended)
Download the client, install its dependencies then run the angular server.
```
git clone https://gitlab.com/itsvinnie/bored-client.git
npm install
ng serve
```

### with postman / curl
Get a random activity:
```
curl -X GET 'http://localhost:8000'
```
Specify some filters:
```
curl -X GET 'http://localhost:8000/?price=0&participants=2'
```

## Improvement
Some features to add:
- create an account: keep track of the activities that a user creates
- let users select a random activity among the activities they have created
- make the frontend a progressive web app that keeps the user's favorite activities
- add a picture for each type of activities